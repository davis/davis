# DAVIS : Distributed Algorithms VISualization

![udavis.png](doc/udavis.png)



## Binary:
`udavis` : a simple viewer for [`jbotsim`](http://jbotsim.io)

![status](https://gitlab.lis-lab.fr/davis/davis-beta/badges/master/pipeline.svg)

You can get it [here](https://davis.pages.lis-lab.fr/davis-beta/distributions/davis-beta-latest.zip) 

## Dependencies
- known to work with java 8 and 11
- jbotsim  : `jbotsim.jar` (You can get it at http://jbotsim.io and https://repo1.maven.org/maven2/io/jbotsim/)
- `tools.jar` : One need to have a jdk installed in order to get tools.jar. The jdk version should be the same as the jre (ex openjdk-8-jdk for openjdk-8-jre).

## Hello world and documentation

See *Hello World* example at https://gitlab.lis-lab.fr/davis/basic-example

```
git clone https://gitlab.lis-lab.fr/davis/basic-example
```

Documentation is [there](doc/en/doc.md)
and [là en français](doc/fr/doc.md).

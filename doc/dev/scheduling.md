# Scheduling of algorithms in **udavis**

## Theory    

**Initialization**    
* *Create* the **Topology** and the **Nodes**    
* *Affect the Ids* to the **Nodes**    
* *Start* the **Topology**    
* *Start* the **Nodes**    
* *Initialize* the **Nodes**    
	- *Set the initial state*    
	- Action of this initial state, with the sending of the first messages    

**Round**    
* The **Nodes** *receive the messages*    
* They *act* according to their state and the message they receive    
	- they *send messages*    
	- they may *change their state*    
* They may *act* according to their state    

## Effective scheduling    

**Initialization**    
* *Create* the **Topology**    
* *Launch* the **NetworkGenerator**    
	-> *Create* the **Nodes** according to the **network** given    
	-> *Affect the Ids* to the **Nodes** according to the given **id scheme**    
* *Start* the **Topology** (immediately if no GUI, else when the user decides : START or STEP)    
	-> *activate* the **OnStartListeners**    
	-> *start* the **Nodes**    
* *Initialize* the **Nodes**    
	-> *set the initial state*   

**Round**    
* On *PreClock* :    
	-> The **Nodes** *act* according to their state if a method without argument exists    (including init)
	-> The *new state* is effectively applied    
* The **Nodes** *receive the messages*    
	-> They *act* according to their state and the message they received    
* **Nodes** action :    
	-> They *send messages*    
	-> They prepare to *change to a new state*    

## In the udavis code    

**Initialization**    
* `topo = new Topology(...);`    
* `networkGenerator.generate(topo,idscheme);`    
* `topo.restart();` in order to *activate* the **OnStartListeners** *before start* the **Nodes**    
	-> the **Nodes** are *started*, but a *false start* has been created    
* `topo.start();` to *really start* the **Nodes**    
	-> `node.onStart()` calls `node.init()` : the **Nodes** are here *initialized*    
	-> `node.init()` shall call `node.become(firstState)` in order to *set the initial state*    
	-> this first `become(...)` calls `node.onPreClock()` to *effectively set the initial state*    

**Round**    
* `node.onPreClock()`    
	-> `state();` if it exists (may call `send(Node,Message);` and `become(newstate);`)    
	-> `state = newstate;`    
* `node.onMessage()`    
	-> add the message to the mailbox : the message animation can then begin    
	-> `state(Message);` may call `send(Node,Message);` and `become(newstate);`    

**Message Animation**    (removed in 0.5)
This animation is scheduled through a **LinkPainter**. The animation begins only after the message has been added to the **Node**'s mailbox and consists in 5 steps of printing :    
* *AnimStart* when the animation is started : a point is painted on (actually behind) the sender and the content is written near    
* *FirstPrint* : the point and the content are painted at 1/4 of the link    
* *MiddlePrint* : they are painted at 1/2 of the link    
* *LastPrint* : they are painted at 3/4 of the link    
* *AnimEnd* the animation ends here : the point and the content are painted on (actually behind) the receiver, then the message is removed from the **Node**'s mailbox    

Actually, if the topology is running step by step, there is an other step before *AnimEnd* : *AnimWaiting*. This step is exactly the same as *AnimEnd*, but the animation doesn't stop (ie the message is still printed) until the next round of simulation has begun.    

The **LinkPainters** are applied in `paintComponent(Graphic g)` of JTopology, inherited from JPanel of **javax.swing**. So the start and the frequency of painting are not under control.    

**How the algorithm waits for the end of the message animation**    
The `onMessage()` method set the algorithm's status to *WAITING_MSG_PAINTING* after adding the message to the mailbox. While the status is not *RUNNING*, the **Nodes** won't do anything during the rounds, and the rounds won't be incremented. This let the animation run. When the animation is over, the algorithm's status is set to *RESUMING_AFTER_MSG_PAINTING*. Then a `ClockListener` sets it to *RUNNING*, tells every **Nodes** to process their messages and finally print the head of the next round. And so on.    

## In the *jbotsim* code    

**Initialization**
* `topo.start();` calls `topo.restart();` which *starts* the **Nodes** and *then activates* the **OnStartListeners**    

**Round**    
* `onClock()` of the **MessageEngine** which *transmit the sent messages* to their destination and then calls the `onMessage()` of the destination **Node** (si `MessageEngine` par défaut).    
* `onClock()` of the **NodeScheduler** which calls in this order :    
	- `onPreClock()` of every **Node**    
	- `onClock()` of every **Node**    
	- `onPostClock()` of every **Node**    
* `onClock()` of the **Topology** which *update* the GUI    
* the `onClock()` of every **ClockListener**

## Improvements and Issues

**For jbotsim**
* The **OnStartListeners** have also an `onPreStart()` method called before the `onStart()` of **Nodes** and rename the `onStart()` in `onPostStart()`    
* The **ClockListeners** also have an `onPreClock()` and an `onPostClock()` methods
* The `onPreClock()` of **Nodes** called at the beginning of the round    
* The `onPostClock()` of **Nodes** called at the end of the round

**For udavis**
* `state()` of **Nodes** called in `onPostClock()`    

***

/*
 Bidirectionnal Quadratic Leader Election Algorithm
*/

// Works only on rings
import davis.core.model.RingNode;

public class BQElection extends RingNode {
  Integer leader = 0; // uid of the leader
  Integer uid = 0; // unset

  // Message classes
  class Left { // left (counterclockwise) messages
    Integer n; // content

    public Left(Integer n) {
      this.n = n;
    }

    public String toString() { // for println
      return n.toString();
    }
  };

  class Right { // right (clockwise) messages
    Integer n; // content

    public Right(Integer n) {
      this.n = n;
    }

    public String toString() { // for println
      return n.toString();
    }
  };

  // Initialization method (MANDATORY)
  public void init() {
    uid = getID(); // init id
    leader = uid;
    sendLeft(new Left(uid)); // send own id to left
    sendRight(new Right(uid)); // send own id to right
    become("active"); // initial state
  }

  public void active(Left msg) { // message from right
    if (msg.n > uid) { // not leader
      log("Je ne suis PAS ELU...");
      become("follower");
    }
    if (msg.n == uid) { // own message
      log("Je suis ELU!");
      become("elected");
      log("Nb runnings : " + getRunnings());
    } else { // sending to next node
      sendLeft(msg); // send to left
    }
  }

  public void active(Right msg) { // message from left
    if (msg.n > uid) { // not leader
      log("Je ne suis PAS ELU...");
      become("follower");
    }
    if (msg.n == uid) { // own message
      log("Je suis ELU!");
      become("elected");
      log("Nb runnings : " + getRunnings());
    } else { // sending to next node
      sendRight(msg); // send to right
    }
  }

  public void follower(Left msg) { // message from right
    if (msg.n == uid) { // own message
      log("TOUR COMPLET");
    } else { // sending to next node
      leader = Math.max(leader, msg.n); // update leader id
      sendLeft(msg); // send to left
    }
  }

  public void follower(Right msg) { // message from left
    if (msg.n == uid) { // own message
      log("TOUR COMPLET");
      terminate(); // local termination
      log("Nb runnings : " + getRunnings());
    } else { // sending to next node
      leader = Math.max(leader, msg.n); // update leader id
      sendRight(msg); // send to right
    }
  }

  public void elected() {
    terminate(); // local termination
    log("Nb runnings : " + getRunnings());
  }
}
